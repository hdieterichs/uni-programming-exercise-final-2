package final2.de.hediet;

import static final2.de.hediet.Program.initializeDocumentContainer;
import final2.de.hediet.documents.Document;
import final2.de.hediet.documents.DocumentContainer;
import final2.de.hediet.documents.MultilineParseException;
import final2.de.hediet.filesystem.Directory;
import final2.de.hediet.filesystem.DirectoryBuilder;
import final2.de.hediet.tags.Tag;
import java.io.FileReader;
import java.io.IOException;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 * @author Henning
 */
public class DocumentParserNGTest {

    /**
     *
     */
    public DocumentParserNGTest() {
    }

    /**
     *
     * @throws IOException a
     * @throws MultilineParseException a
     */
    @Test
    public void test1() throws IOException, MultilineParseException {
        FileReader fr = new FileReader("");

        DocumentContainer fileSystem = initializeDocumentContainer(fr);

        for (Document d : fileSystem.getDocumentCollection()) {
            for (Tag t : fileSystem.getTagCollection()) {
                System.out.println(d + " " + d.getTagValue(t));
            }
        }

        DirectoryBuilder a = new DirectoryBuilder(System.out);
        Directory dir = a.buildDirectory(fileSystem.getDocumentCollection(), fileSystem.getTagCollection());
        System.out.println("---");
        System.out.println(dir.contentToString());
    }

}
