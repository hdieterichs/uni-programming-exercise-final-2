package final2.de.hediet.filesystem;

/**
 * Represents an abstract node.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public abstract class Node {

    /**
     * Writes the content of this node to {@code stringBuilder}.
     *
     * @param stringBuilder the string builder to write to. Cannot be null.
     */
    protected abstract void writeContentToStringBuilder(StringBuilder stringBuilder);

    /**
     * Gets the result of of {@link #writeContentToStringBuilder} as string.
     *
     * @return the result of {@link #writeContentToStringBuilder} as string.
     * Will not be null.
     */
    public final String contentToString() {
        StringBuilder sb = new StringBuilder();
        writeContentToStringBuilder(sb);

        return sb.toString();
    }

    /**
     * Gets the path of this node.
     *
     * @return the path of this node. Will not be null.
     */
    public abstract String getPath();
}
