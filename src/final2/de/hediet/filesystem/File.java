package final2.de.hediet.filesystem;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.documents.Document;

/**
 * Represents a file which refers to a document.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class File extends Node {

    private final Directory parent;
    private final Document document;

    /**
     * Creates a new file.
     *
     * @param parent the parent directory. Can be null.
     * @param document the document this file will containt. Cannot be null.
     */
    public File(Directory parent, Document document) {
        ArgumentExceptionHelper.ensureNotNull(document, "document");

        this.parent = parent;
        this.document = document;
    }

    /**
     * Gets the document of this file.
     *
     * @return the document. Will not be null.
     */
    public Document getDocument() {
        return document;
    }

    /**
     * Gets the path to this file. The path will be the path of the parent
     * combined with the name of the document.
     *
     * @return the path of this file. Will not be null.
     */
    @Override
    public String getPath() {
        return String.format("%s/\"%s\"", parent.getPath(), getDocument().getName());
    }

    /**
     * Writes the path of this file to {@code stringBuilder}.
     * After the path, a line separator will be appended.
     *
     * @param stringBuilder the string builder to write to. Cannot be null.
     */
    @Override
    protected void writeContentToStringBuilder(StringBuilder stringBuilder) {
        ArgumentExceptionHelper.ensureNotNull(stringBuilder, "stringBuilder");

        stringBuilder.append(getPath()).append(System.lineSeparator());
    }
}
