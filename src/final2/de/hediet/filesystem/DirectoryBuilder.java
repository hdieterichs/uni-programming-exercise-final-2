package final2.de.hediet.filesystem;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.documents.Document;
import final2.de.hediet.tags.Tag;
import final2.de.hediet.tags.TagValue;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This class builds a directory structure from given documents and tags.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class DirectoryBuilder {

    private static final double EPSILON = 10E-3;
    private final PrintStream loggerStream;

    /**
     * Creates a new directory builder.
     *
     * @param loggerStream the stream the algorithm will write the log to. Can
     * be null.
     */
    public DirectoryBuilder(PrintStream loggerStream) {
        this.loggerStream = loggerStream;
    }

    /**
     * Builds a directory tree.
     *
     * @param documents the documents. Cannot be null.
     * @param allTags all tags considered in this and all child directories.
     * Cannot be null.
     * @return the created directory.
     */
    public Directory buildDirectory(Iterable<Document> documents, Iterable<Tag> allTags) {
        ArgumentExceptionHelper.ensureNotNull(documents, "documents");
        ArgumentExceptionHelper.ensureNotNull(allTags, "allTags");

        return buildDirectory(documents, allTags, null, null);
    }

    private Directory buildDirectory(Iterable<Document> documents,
            Iterable<Tag> allTags, Directory parent, TagValue directoryLabel) {
        assert documents != null;
        assert allTags != null;

        Collection<Node> subNodes = new ArrayList<>(); //the subNodes will be added later
        Directory curDir = new Directory(parent, subNodes, directoryLabel);

        Tag bestTag = null;
        double highestInformationGain = 0;
        for (Tag tag : allTags) {
            double informationGain = getExpectedInformationGain(documents, tag);

            if (informationGain > EPSILON && loggerStream != null) {
                loggerStream.println(String.format("%s/%s=%.2f",
                        curDir.getPath(), tag.getName(), informationGain));
            }

            if (informationGain > highestInformationGain) {
                highestInformationGain = informationGain;
                bestTag = tag;
            }
        }

        if (highestInformationGain < EPSILON) {
            for (Document d : documents) {
                subNodes.add(new File(curDir, d));
            }
            return curDir;
        }

        assert bestTag != null : "highestInformationGain >= EPSILON implies at least one bestTag was set";

        Set<TagValue> distinctTagValues = new HashSet<>();
        for (Document document : documents) {
            distinctTagValues.add(document.getTagValue(bestTag));
        }

        for (TagValue tagValue : distinctTagValues) {
            Collection<Document> subDocuments = new ArrayList<>();

            //add all documents whose bestTag tag value is equal to tagValue.
            for (Document document : documents) {
                if (document.getTagValue(bestTag).equals(tagValue)) {
                    subDocuments.add(document);
                }
            }

            subNodes.add(buildDirectory(subDocuments, allTags, curDir, tagValue));
        }

        return curDir;
    }

    private static double getExpectedInformationGain(Iterable<Document> set, Tag tag) {
        assert set != null;
        assert tag != null;

        return getUncertainty(set, null) - getRemainingUncertainty(set, tag);
    }

    private static double getRemainingUncertainty(Iterable<Document> set, Tag tag) {
        assert set != null;
        assert tag != null;

        double result = 0;
        Set<TagValue> values = new HashSet<>();
        for (Document d : set) {
            values.add(d.getTagValue(tag));
        }

        for (TagValue v : values) {
            result += getProbability(set, v) * getUncertainty(set, v);
        }

        return result;
    }

    private static double getProbability(Iterable<Document> set, TagValue tagValue) {
        assert set != null;
        assert tagValue != null;

        double value = 0;
        double sum = 0;
        for (Document d : set) {
            sum += d.getAccessCount();
            if (d.getTagValue(tagValue.getAssociatedTag()).equals(tagValue)) {
                value += d.getAccessCount();
            }
        }

        return value / sum;
    }

    /**
     * Gets the uncertainty of {@code set}. The set if filtered by tagValue if
     * tagValue is not null.
     *
     * @param set the set. Cannot be null.
     * @param tagValue the tag value which will filter the set. Can be null.
     * @return the uncertainty.
     */
    private static double getUncertainty(Iterable<Document> set, TagValue tagValue) {
        assert set != null;

        double sum = 0;
        for (Document d : set) {
            if (tagValue == null
                    || d.getTagValue(tagValue.getAssociatedTag()).equals(tagValue)) {
                sum += d.getAccessCount();
            }
        }
        double result = 0;
        for (Document d : set) {
            if (tagValue == null
                    || d.getTagValue(tagValue.getAssociatedTag()).equals(tagValue)) {
                double p = d.getAccessCount() / sum;
                if (p != 0) {
                    result -= p * Math.log(p) / Math.log(2);
                }
            }
        }

        return result;
    }
}
