package final2.de.hediet.filesystem;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.tags.TagValue;

/**
 * Represents a directory of nodes.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class Directory extends Node {

    private final Iterable<Node> subNodes;
    private final TagValue directoryLabel;
    private final String path;

    /**
     * Creates a new directory.
     *
     * @param parent the parent directory. Can be null.
     * @param subNodes the sub nodes. Cannot be null. Its items can be changed
     * after construction.
     * @param directoryLabel the label of this directory. Can be null.
     */
    public Directory(Directory parent, Iterable<Node> subNodes, TagValue directoryLabel) {
        ArgumentExceptionHelper.ensureNotNull(subNodes, "subNodes");

        this.subNodes = subNodes;
        this.directoryLabel = directoryLabel;

        if (directoryLabel == null) {
            path = "";
        } else {
            path = String.format("%s/%s=%s",
                    parent != null ? parent.getPath() : "",
                    directoryLabel.getAssociatedTag().getName(),
                    directoryLabel.getValueAsString());
        }
    }

    /**
     * Gets the sub nodes of this directory.
     *
     * @return the list of nodes. Will not be null.
     */
    public Iterable<Node> getNodes() {
        return subNodes;
    }

    /**
     * Gets the label of this directory.
     *
     * @return the label. Can be null.
     */
    public TagValue getDirectoryLabel() {
        return directoryLabel;
    }

    /**
     * Gets the path. The path will be the path of the parent combined with the
     * directory label of this directory.
     *
     * @return the path of this directory. Will not be null.
     */
    @Override
    public String getPath() {
        return path;
    }

    /**
     * Writes the content of all subnodes to {@code stringBuilder}, so that the
     * whole directory tree will be printed recursively.
     *
     * @param stringBuilder the string builder to write to. Cannot be null.
     */
    @Override
    protected void writeContentToStringBuilder(StringBuilder stringBuilder) {
        ArgumentExceptionHelper.ensureNotNull(stringBuilder, "stringBuilder");

        for (Node n : getNodes()) {
            n.writeContentToStringBuilder(stringBuilder);
        }
    }
}
