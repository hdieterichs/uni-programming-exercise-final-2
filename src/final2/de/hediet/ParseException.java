package final2.de.hediet;

/**
 * Represents a parse exception for single-line text to indicate a malformed
 * text block.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ParseException extends Exception {

    private final int startOffset;
    private final int endOffset;

    /**
     * Creates a new parse exception. {@code startOffset} has to be greater than
     * or equal to 0, {@code endOffset} has to be greater than or equal to
     * {@code startOffset}.
     *
     * @param message the message. Cannot be null.
     * @param startOffset the offset where the malformed text begins.
     * @param endOffset the offset of the first character after the malformed
     * text block.
     */
    public ParseException(String message, int startOffset, int endOffset) {
        super(message);

        ArgumentExceptionHelper.ensureNotNull(message, "message");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(startOffset, 0, "startOffset");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(endOffset, startOffset, "endOffset");

        this.startOffset = startOffset;
        this.endOffset = endOffset;
    }

    /**
     * Gets the offset where the malformed text starts. Will be greater than or
     * equal to 0.
     *
     * @return the offset, where the malformed text starts.
     */
    public int getStartOffset() {
        return startOffset;
    }

    /**
     * Gets the offset of the first character after the malformed text block.
     * Will be greater than or equals to {@link #getStartOffset()}. If it is
     * equal to {@link #getStartOffset()}, no malformed text is enclosed. This
     * may indicate that characters were missing.
     *
     * @return the offset of the first character after the malformed text block.
     */
    public int getEndOffset() {
        return endOffset;
    }
}
