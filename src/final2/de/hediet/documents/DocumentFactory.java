package final2.de.hediet.documents;

import final2.de.hediet.TagException;
import final2.de.hediet.tags.TagValueProvider;

/**
 * Represents an abstract document factory.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public abstract class DocumentFactory {

    /**
     * Creates a new document.
     *
     * @param documentType the requested document type. Cannot be null.
     * @param documentName the name the returned document must have. Cannot be
     * null.
     * @param accessCount the number of accesses to the document. Has to be
     * greater than or equal to 0.
     * @param tagValueProvider the tag value provider the document can use to
     * create new tag values. Cannot be null.
     * @return the document. Will be null, if the requested document cannot be
     * created.
     * @throws TagException will be thrown, if tag related operations fail.
     */
    public abstract Document createDocument(String documentType,
            String documentName, int accessCount, TagValueProvider tagValueProvider) throws TagException;
}
