package final2.de.hediet.documents.tagValueTransformations;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.ParseException;
import final2.de.hediet.TagException;
import final2.de.hediet.tags.MultivalentTag;
import final2.de.hediet.tags.TagValue;
import final2.de.hediet.tags.TagValueProvider;

/**
 * Represents a multivalent value rename transformation which transforms
 * multivalent tag values with a specified name to a tag value with the same
 * value but different name. The new tag value will be requested on demand from
 * the tag value provider. This transformation can be chained.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class MultivalentValueRenameTransformation extends Transformation {

    private final String sourceTagName;
    private final String targetTagName;
    private final Transformation next;

    /**
     * Creates a new multivalent value rename transformation.
     *
     * @param sourceTagName the source tag name. Cannot be null.
     * @param targetTagName the target tag name. Cannot be null.
     * @param next the next transformation. Can be null.
     */
    public MultivalentValueRenameTransformation(String sourceTagName, String targetTagName,
            Transformation next) {
        ArgumentExceptionHelper.ensureNotNull(sourceTagName, "sourceTagName");
        ArgumentExceptionHelper.ensureNotNull(targetTagName, "targetTagName");

        this.sourceTagName = sourceTagName.toLowerCase();
        this.targetTagName = targetTagName;
        this.next = next;

    }

    /**
     * Renames a given tag value. A tag value will be renamed, if it is
     * multivalent and the name of the associated tag matches to
     * {@code sourceTagName}, which was passed to the constructor. In all cases,
     * the returned value will be transformed by the chained transformation if
     * it was set.
     *
     * @param tagValue the tag value to transform. Cannot be null.
     * @param tagValueProvider the tag value provider used to create new tag
     * values. Cannot be null.
     * @return the transformed tag value. Will not be null.
     * @throws TagException will be thrown, if the tag value provider cannot
     * provide the new tag value.
     */
    @Override
    public TagValue transform(TagValue tagValue, TagValueProvider tagValueProvider) throws TagException {
        TagValue result = tagValue;
        if (tagValue instanceof MultivalentTag.MultivalentTagValue
                && tagValue.getAssociatedTag().getName().toLowerCase().equals(sourceTagName)) {
            try {
                result = tagValueProvider.getTagValue(targetTagName, tagValue.getValueAsString());
            } catch (ParseException e) {
                throw new TagException(String.format("Tag '%s' is not a multivalent tag.", targetTagName));
            }
        }

        if (next != null) {
            result = next.transform(result, tagValueProvider);
        }
        return result;
    }

}
