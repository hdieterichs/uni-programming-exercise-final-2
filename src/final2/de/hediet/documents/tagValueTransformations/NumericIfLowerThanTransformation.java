package final2.de.hediet.documents.tagValueTransformations;

/**
 * Represents a transformation which will translate numeric tag values to a
 * predefined tag value, if they are lower than a reference value.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class NumericIfLowerThanTransformation
        extends ConditionalNumericTransformation {

    private final int valueToCompareAgainst;

    /**
     * Creates an new numeric if lower than transformation.
     *
     * @param tagName the tag name. Cannot be null.
     * @param newTagName the new tag name. Cannot be null.
     * @param valueToCompareAgainst the reference value.
     * @param newTagValue the new tag value. Can be null.
     * @param next the next transformation. Can be null.
     */
    public NumericIfLowerThanTransformation(String tagName, String newTagName,
            int valueToCompareAgainst, String newTagValue, Transformation next) {
        super(tagName, newTagName, newTagValue, next);

        this.valueToCompareAgainst = valueToCompareAgainst;
    }

    /**
     * Checks whether {@code value} is lower than the reference value.
     *
     * @param value the reference value.
     * @return true, if {@code value} &lt; {@code valueToCompareAgainst}, otherwise
     * false.
     */
    @Override
    protected boolean checkCondition(int value) {
        return value < valueToCompareAgainst;
    }
}
