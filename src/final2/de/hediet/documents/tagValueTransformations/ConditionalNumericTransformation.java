package final2.de.hediet.documents.tagValueTransformations;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.ParseException;
import final2.de.hediet.TagException;
import final2.de.hediet.tags.NumericTag;
import final2.de.hediet.tags.TagValue;
import final2.de.hediet.tags.TagValueProvider;

/**
 * Represents an abstract conditional numeric transformation, which transforms
 * numeric tag values to a predefined tag value if a certain condition is met.
 * The condition can be specified in derived classes. Only tag values with a
 * specified name will be transformed. The predefined tag value will be
 * requested on demand from the tag value provider. This transformation can be
 * chained.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public abstract class ConditionalNumericTransformation extends Transformation {

    private final String tagName;
    private final String newTagName;
    private final String newTagValue;
    private final Transformation next;

    /**
     * Creates a new conditional numeric transformation.
     *
     * @param tagName the tag name of the tag values to transform. Cannot be
     * null.
     * @param newTagName the tag name of the transformed tag value. Cannot be
     * null.
     * @param newTagValue the value of the transformed tag value. Can be null.
     * @param next the next transformation which will be applied to the
     * transformed tag value. Can be null.
     */
    protected ConditionalNumericTransformation(
            String tagName, String newTagName, String newTagValue, Transformation next) {
        ArgumentExceptionHelper.ensureNotNull(tagName, "tagName");
        ArgumentExceptionHelper.ensureNotNull(newTagName, "newTagName");

        this.tagName = tagName.toLowerCase();
        this.newTagName = newTagName;
        this.newTagValue = newTagValue;
        this.next = next;
    }

    /**
     * Transforms a given tag value. A tag value will be transformed, if it is
     * numeric, the name of the associated tag matches to {@code tagName}, which
     * was passed to the constructor, and if {@link #checkCondition(int)}
     * returns true for the old tag value. In all cases, the returned value will
     * be transformed by the chained transformation if it was set.
     *
     * @param tagValue the tag value to transform. Cannot be null.
     * @param tagValueProvider the tag value provider used to create new tag
     * values. Cannot be null.
     * @return the transformed tag value. Will not be null.
     * @throws TagException will be thrown, if the tag value provider cannot
     * provide the new tag value.
     */
    @Override
    public TagValue transform(TagValue tagValue, TagValueProvider tagValueProvider) throws TagException {

        TagValue result = tagValue;

        if (tagValue instanceof NumericTag.NumericTagValue
                && tagValue.getAssociatedTag().getName().toLowerCase().equals(tagName)) {
            int tagValueInt = ((NumericTag.NumericTagValue) tagValue).getValue();
            if (checkCondition(tagValueInt)) {
                try {
                    result = tagValueProvider.getTagValue(newTagName, newTagValue);
                } catch (ParseException e) {
                    throw new TagException(String.format(
                            "New tag value '%s' is not compatible to the tag '%s'.", newTagValue, newTagName));
                }
            }
        }

        if (next != null) {
            result = next.transform(result, tagValueProvider);
        }

        return result;
    }

    /**
     * Checks the condition. The transformation will be applied only to numeric
     * tag values for which this condition returns {@code true}.
     *
     * @param value the value to check.
     * @return true if the condition is met, otherwise false.
     */
    protected abstract boolean checkCondition(int value);
}
