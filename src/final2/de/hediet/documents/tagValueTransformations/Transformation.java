package final2.de.hediet.documents.tagValueTransformations;

import final2.de.hediet.TagException;
import final2.de.hediet.tags.TagValue;
import final2.de.hediet.tags.TagValueProvider;

/**
 * Represents a transformation of tag values.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public abstract class Transformation {

    /**
     * Transforms a given tag value.
     *
     * @param tagValue the tag value to transform. Cannot be null.
     * @param tagValueProvider the tag value provider used to create now tag
     * values. Cannot be null.
     * @return the transformed tag value. Will not be null.
     * @throws TagException will be thrown, if the tag value provider cannot
     * provide the requested tag value.
     */
    public abstract TagValue transform(TagValue tagValue,
            TagValueProvider tagValueProvider) throws TagException;
}
