package final2.de.hediet.documents.tagValueTransformations;

/**
 * Represents a transformation which will translate numeric tag values to a
 * predefined tag value, if they are greater than or equal to a reference value.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class NumericIfGreaterThanOrEqualTransformation
        extends ConditionalNumericTransformation {

    private final int valueToCompareAgainst;

    /**
     * Creates an new numeric if greater than or equal transformation.
     *
     * @param tagName the tag name. Cannot be null.
     * @param newTagName the new tag name. Cannot be null.
     * @param valueToCompareAgainst the reference value.
     * @param newTagValue the new tag value. Can be null.
     * @param next the next transformation. Can be null.
     */
    public NumericIfGreaterThanOrEqualTransformation(String tagName, String newTagName,
            int valueToCompareAgainst, String newTagValue, Transformation next) {
        super(tagName, newTagName, newTagValue, next);

        this.valueToCompareAgainst = valueToCompareAgainst;
    }

    /**
     * Checks whether {@code value} is greater than or equal to the reference value.
     *
     * @param value the reference value.
     * @return true, if {@code value} &gt;= {@code valueToCompareAgainst}, otherwise
     * false.
     */
    @Override
    protected boolean checkCondition(int value) {
        return value >= valueToCompareAgainst;
    }
}
