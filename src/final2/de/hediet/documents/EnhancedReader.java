package final2.de.hediet.documents;

import final2.de.hediet.ArgumentExceptionHelper;
import java.io.IOException;
import java.io.Reader;

/**
 * An enhanced reader which allows to peek the next character without moving
 * forward. This reader keeps track of the current line number and column.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
class EnhancedReader {

    private final Reader underlayingReader;
    private int line = 1;
    private int column = 1;
    private int peekedChar;
    private boolean hasPeekedChar = false;
    private boolean isAtEnd = false;

    /**
     * Creates a new enhanced reader.
     *
     * @param underlayingReader the underlaying reader. Cannot be null.
     */
    public EnhancedReader(Reader underlayingReader) {
        ArgumentExceptionHelper.ensureNotNull(underlayingReader, "underlayingReader");
        this.underlayingReader = underlayingReader;
    }

    /**
     * Gets the one-based line.
     *
     * @return the current line.
     */
    public int getLine() {
        return line;
    }

    /**
     * Gets the one-based column.
     *
     * @return the current column.
     */
    public int getColumn() {
        return column;
    }

    /**
     * Gets whether the reader is at the end. If the reader is at the end, call
     * to read and peek will always return 0.
     *
     * @return true, if the reader is at the end, otherwise false.
     */
    public boolean isAtEnd() {
        return isAtEnd;
    }

    /**
     * Peeks the next character. This call does not change the state of this
     * instance. 0 is returned, if there is no next character.
     *
     * @return the next character.
     * @throws IOException will be thrown, if the underlaying reader throws an
     * IOException.
     */
    public char peek() throws IOException {
        return nextChar(true);
    }

    /**
     * Reads the next character. 0 is returned, if there is no next character.
     *
     * @return the next character.
     * @throws IOException will be thrown, if the underlaying reader throws an
     * IOException.
     */
    public char read() throws IOException {
        return nextChar(false);
    }

    private char nextChar(boolean peek) throws IOException {
        int readChar;

        if (hasPeekedChar) {
            //if the next character was cached
            readChar = peekedChar;
        } else {
            readChar = underlayingReader.read();

            if (peek) {
                hasPeekedChar = true;
                peekedChar = readChar;
            }
        }

        char result;
        if (readChar == -1) {
            result = 0;
            if (!peek) {
                isAtEnd = true;
            }
        } else {
            result = (char) readChar;
        }

        if (!peek) {
            hasPeekedChar = false;
            //set column and line, but only if the current character is not beeing peeked.

            //line breaks are \n, \r and \r\n. \r\n will not be matched here, 
            //because \n will count also as line break and \r\n is only one line break.
            if (result == '\n' || (result == '\r' && peek() != '\n')) {
                column = 1;
                line++;
            } else {
                column++;
            }
        }

        return result;
    }
}
