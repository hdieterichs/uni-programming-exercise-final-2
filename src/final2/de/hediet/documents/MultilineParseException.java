package final2.de.hediet.documents;

import final2.de.hediet.ArgumentExceptionHelper;

/**
 * Represents a multiline parse exception to indicate malformed text blocks over
 * multiple lines.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class MultilineParseException extends Exception {

    private final int startLine;
    private final int startColumn;
    private final int endLine;
    private final int endColumn;

    /**
     * Creates a new multiline parse exception.
     *
     * @param message the message of this exception.
     * @param startLine the start line. Has to be greater than or equal to 1.
     * @param startColumn the start column. Has to be greater than or equal to
     * 1.
     * @param endLine the end line. Has to be greater than or equal to
     * {@code startLine}.
     * @param endColumn the end column. Has to be greater than or equal to
     * {@code startColumn} if {@code startLine} is equal to {@code endLine}.
     * Otherwise it has to be greater than or equal to 1.
     */
    public MultilineParseException(String message,
            int startLine, int startColumn, int endLine, int endColumn) {
        super(buildMessage(message, startLine, startColumn, endLine, endColumn));

        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(startLine, 1, "startLine");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(startColumn, 1, "startColumn");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(endLine, startLine, "endLine");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(endColumn, 1, "endColumn");
        if (startLine == endLine) {
            ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(endColumn, startColumn, "endColumn");
        }

        this.startLine = startLine;
        this.startColumn = startColumn;
        this.endLine = endLine;
        this.endColumn = endColumn;
    }

    private static String buildMessage(String message,
            int startLine, int startColumn, int endLine, int endColumn) {

        String suffix;
        if (startLine == endLine) {
            if (startColumn == endColumn) {
                suffix = String.format("(in line %d, column %d).",
                        startLine, startColumn);
            } else {
                suffix = String.format("(in line %d, from column %d to %d).",
                        startLine, startColumn, endColumn);
            }
        } else {
            suffix = String.format("(from line %d column %d to line %d column %d).",
                    startLine, endLine, startColumn, endColumn);
        }
        String newMessage = message;

        //remove "." at the end of message, since suffix already ends with ".".
        if (newMessage.endsWith(".")) {
            newMessage = newMessage.substring(0, message.length() - 1);
        }

        return newMessage + " " + suffix;
    }

    /**
     * Gets the start line. Will be greater than or equal to 1.
     *
     * @return the start line.
     */
    public int getStartLine() {
        return startLine;
    }

    /**
     * Gets the start column. Will be greater than or equal to 1.
     *
     * @return the start column.
     */
    public int getStartColumn() {
        return startColumn;
    }

    /**
     * Gets the end line. Will be greater than or equal to
     * {@link #getStartLine()}.
     *
     * @return the end line.
     */
    public int getEndLine() {
        return endLine;
    }

    /**
     * Gets the end column. Will be greater than or equal to
     * {@link #getStartColumn()}.
     *
     * @return the end column.
     */
    public int getEndColumn() {
        return endColumn;
    }
}
