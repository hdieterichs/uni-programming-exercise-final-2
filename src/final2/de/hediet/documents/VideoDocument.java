package final2.de.hediet.documents;

import final2.de.hediet.documents.tagValueTransformations.MultivalentValueRenameTransformation;
import final2.de.hediet.documents.tagValueTransformations.NumericIfGreaterThanOrEqualTransformation;
import final2.de.hediet.documents.tagValueTransformations.NumericIfLowerThanTransformation;
import final2.de.hediet.documents.tagValueTransformations.Transformation;
import final2.de.hediet.tags.TagValueProvider;

/**
 * Represents an video document. This document uses the tag transformation rules
 * as described in the excercise sheet.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class VideoDocument extends TransformationDocument {

    /**
     * Creates a new video document.
     *
     * @param name the name of the document. Cannot be null.
     * @param accessCount the number of accesses to this document. Has to be
     * greater than or equal to 0.
     * @param tagValueProvider the provider which will be used by transformation
     * rules to obtain new tag values. Cannot be null.
     */
    public VideoDocument(String name, int accessCount, TagValueProvider tagValueProvider) {
        super(name, accessCount, tagValueProvider, getTransformation());
    }

    private static Transformation getTransformation() {
        Transformation result = new NumericIfGreaterThanOrEqualTransformation(
                "Length", "VideoLength", 7200, "Long", null);
        result = new NumericIfLowerThanTransformation(
                "Length", "VideoLength", 7200, "Movie", result);
        result = new NumericIfLowerThanTransformation(
                "Length", "VideoLength", 3600, "Short", result);
        result = new NumericIfLowerThanTransformation(
                "Length", "VideoLength", 300, "Clip", result);
        result = new MultivalentValueRenameTransformation(
                "Genre", "VideoGenre", result);
        return result;
    }
    
    /**
     * Creates a factory for this document. This factory will handle the
     * document type "video".
     *
     * @param nextDocumentFactory the next factory which will be used if this
     * factory cannot create the requested document. Can be null.
     * @return a factory for this document. Will not be null.
     */
    public static DocumentFactory createDocumentFactory(DocumentFactory nextDocumentFactory) {
        return new ChainedDocumentFactory("video", nextDocumentFactory) {
            @Override
            protected Document createDocument(String documentName, 
                    int accessTimes, TagValueProvider tagValueProvider) {
                return new VideoDocument(documentName, accessTimes, tagValueProvider); 
            }
        };
    }
}
