package final2.de.hediet.documents;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.ParseException;
import final2.de.hediet.tags.Tag;
import final2.de.hediet.tags.TagFactory;
import final2.de.hediet.tags.TagValue;
import final2.de.hediet.tags.TagValueProvider;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Represents a document container. A document container composes
 * {@link TagFactory} instances, {@link Tag} instances and {@link Document}
 * instances.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class DocumentContainer implements TagValueProvider {

    private final Collection<Document> documentCollection = new ArrayList<>();
    private final Collection<Tag> tagCollection = new ArrayList<>();
    private final Collection<TagFactory> tagFactories = new ArrayList<>();

    /**
     * Creates a new empty document container.
     */
    public DocumentContainer() {
    }

    /**
     * Gets the document collection. Always the same collection instance will be
     * returned.
     *
     * @return the collection of documents. Will not be null.
     */
    public Collection<Document> getDocumentCollection() {
        return documentCollection;
    }

    /**
     * Gets the tag collection. Always the same collection instance will be
     * returned.
     *
     * @return the collection of tags. Will not be null.
     */
    public Collection<Tag> getTagCollection() {
        return tagCollection;
    }

    /**
     * Gets the tag factory collection. Always the same collection instance will
     * be returned.
     *
     * @return the collection of tag factories. Will not be null.
     */
    public Collection<TagFactory> getTagFactories() {
        return tagFactories;
    }

    /**
     * Gets a tag value of the tag with name {@code tagName} and value
     * {@code value}. If a tag with name {@code tagName} exists, this tag
     * instance will create a tag value by parsing {@code value}. If such a tag
     * does not exists, it will be created by the first tag factory which can
     * parse {@code value}. Newly created tags will be added to the tag
     * collection.
     *
     * @param tagName the name of the tag. Cannot be null.
     * @param value the value. Can be null.
     * @return the parsed tag value. Will not be null.
     * @throws ParseException will be thrown, if either the existing tag cannot
     * parse {@code value}, or there is no tag factory which can parse
     * {@code value}.
     */
    @Override
    public TagValue getTagValue(String tagName, String value) throws ParseException {
        ArgumentExceptionHelper.ensureNotNull(tagName, "tagName");
        
        Tag tag = getTagOrNull(tagName);

        if (tag == null) {
            for (TagFactory f : getTagFactories()) {
                if (f.canParseValue(value)) {
                    tag = f.createTag(tagName);
                    break;
                }
            }
            if (tag == null) {
                throw new ParseException(String.format(
                        "Cannot parse tag value '%s' for tag '%s' (maybe the tag factory is missing)",
                        value, tagName), 0, value.length());
            }

            tagCollection.add(tag);
        }

        TagValue result = tag.parse(value);

        return result;
    }

    private Tag getTagOrNull(String tagName) {
        assert tagName != null;

        for (Tag t : tagCollection) {
            //the tag identifier is case insensitive.
            if (t.getName().toLowerCase().equals(tagName.toLowerCase())) {
                return t;
            }
        }

        return null;
    }
}
