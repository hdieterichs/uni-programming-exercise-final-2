package final2.de.hediet.documents;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.TagException;
import final2.de.hediet.documents.tagValueTransformations.Transformation;
import final2.de.hediet.tags.TagValue;
import final2.de.hediet.tags.TagValueProvider;

/**
 * Represents a document which transforms all tag values which were added to
 * this instance if a transformation is defined.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class TransformationDocument extends Document {

    private final TagValueProvider tagValueProvider;
    private final Transformation tagValueTransformation;

    /**
     * Creates a new transformation document.
     *
     * @param name the name of the document. Cannot be null.
     * @param accessCount the number of accesses to this document. Has to be
     * greater than or equal to 0.
     * @param tagValueProvider the provider which will be used by transformation
     * rules to obtain new tag values. Cannot be null.
     * @param tagValueTransformation the transformation used to transform the
     * tag values which were added to this instance. Can be null.
     */
    public TransformationDocument(String name, int accessCount,
            TagValueProvider tagValueProvider, Transformation tagValueTransformation) {
        super(name, accessCount);

        ArgumentExceptionHelper.ensureNotNull(tagValueProvider, "tagValueProvider");

        this.tagValueProvider = tagValueProvider;
        this.tagValueTransformation = tagValueTransformation;
    }

    /**
     * Adds a tag value to this document. A tag can be specified only once,
     * otherwise a {@link TagException} will be thrown. Before the tag will be
     * added, the tag will be transformed, but only if a transformation is
     * defined.
     *
     * @param tagValue the tag value to add. Cannot be null.
     * @throws TagException will be thrown, if the transformed tag was already defined.
     */
    @Override
    public void addTagValue(TagValue tagValue) throws TagException {
        ArgumentExceptionHelper.ensureNotNull(tagValue, "tagValue");
        
        TagValue newTagValue;
        //only transform if the transformation is defined.
        if (tagValueTransformation != null) {
            newTagValue = tagValueTransformation.transform(tagValue, tagValueProvider);
        } else {
            newTagValue = tagValue;
        }

        super.addTagValue(newTagValue);
    }

}
