package final2.de.hediet.documents;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.TagException;
import final2.de.hediet.tags.TagValue;
import final2.de.hediet.tags.Tag;
import java.util.HashMap;

/**
 * Represents a document. A documents maps {@link Tag} instances to
 * {@link TagValue} instances and consists of a name and an accessCount.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class Document {

    private final String name;
    private final int accessCount;
    private final HashMap<Tag, TagValue> tagValues = new HashMap<>();

    /**
     * Creates a new document.
     *
     * @param name the name of the document. Cannot be null.
     * @param accessCount the number of accesses to this document. Has to be
     * greater than or equal to 0.
     */
    public Document(String name, int accessCount) {
        ArgumentExceptionHelper.ensureNotNull(name, "name");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(accessCount, 0, "accessCount");

        this.name = name;
        this.accessCount = accessCount;
    }

    /**
     * Gets the name of this document.
     *
     * @return the name of this document. Will not be null.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the number of accesses to this document.
     *
     * @return the number of accesses to this document. Will be greater than or
     * equal to 0.
     */
    public int getAccessCount() {
        return accessCount;
    }

    /**
     * Adds a tag value to this document. A tag can be specified only once,
     * otherwise a {@link TagException} will be thrown.
     *
     * @param tagValue the tag value to add. Cannot be null.
     * @throws TagException will be thrown, if the tag was already defined.
     */
    public void addTagValue(TagValue tagValue) throws TagException {
        ArgumentExceptionHelper.ensureNotNull(tagValue, "tagValue");

        TagValue existingTagValue = tagValues.get(tagValue.getAssociatedTag());

        if (existingTagValue != null) {
            throw new TagException(String.format("Tag '%s' already defined with '%s'.",
                    tagValue.getAssociatedTag().getName(), existingTagValue.getValueAsString()));
        }

        tagValues.put(tagValue.getAssociatedTag(), tagValue);
    }

    /**
     * Gets the tag value for the given tag. If the tag was not defined, the
     * undefined tag value will be returned.
     *
     * @param tag the given tag. Cannot be null.
     * @return the tag value. Will not be null.
     */
    public TagValue getTagValue(Tag tag) {
        ArgumentExceptionHelper.ensureNotNull(tag, "tag");

        TagValue result = tagValues.get(tag);
        if (result == null) {
            result = tag.getUndefinedTagValue();
        }
        return result;
    }

    /**
     * Returns a string representation of this document. This string
     * representation will contain the class name and the name of the document.
     *
     * @return the string representation. Will not be null.
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + " " + name;
    }
}
