package final2.de.hediet.documents;

import final2.de.hediet.ParseException;
import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.TagException;
import final2.de.hediet.tags.TagValue;
import final2.de.hediet.tags.TagValueProvider;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * The document parser.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class DocumentParser {

    /**
     * Creates a new document parser.
     */
    public DocumentParser() {
    }

    /**
     * Parses documents. Format is according to the exercise sheet. If the
     * reader contains the 0 char, an exception will be thrown. The identifier
     * of a document has to be unique.
     *
     * @param reader the reader used to read the input stream. Cannot be null.
     * @param documentFactory the document factory used to create new documents.
     * Cannot be null.
     * @param tagValueProvider the tag value provider used to create new tag
     * values. Cannot be null.
     * @return a collection of the parsed documents. Will not be null and will
     * not contain null.
     * @throws IOException will be thrown if the reader throws an IOException.
     * @throws MultilineParseException will be thrown if the documents cannot be
     * parsed.
     */
    public Collection<Document> parseDocuments(
            Reader reader, DocumentFactory documentFactory, TagValueProvider tagValueProvider)
            throws IOException, MultilineParseException {
        ArgumentExceptionHelper.ensureNotNull(reader, "reader");
        ArgumentExceptionHelper.ensureNotNull(documentFactory, "documentFactory");
        ArgumentExceptionHelper.ensureNotNull(tagValueProvider, "tagValueProvider");

        Collection<Document> result = new ArrayList<>();
        Set<String> addedDocumentNames = new HashSet<>();

        EnhancedReader r = new EnhancedReader(reader);

        //0 indicates also end of file.
        while (r.peek() != 0) {
            int currentLine = r.getLine();
            int startColumn = r.getColumn();
            Document d = parseDocument(r, documentFactory, tagValueProvider);
            if (!addedDocumentNames.add(d.getName())) {
                throw createMultilineParseException("document",
                        "A document with name '" + d.getName() + "' already exists.",
                        r, startColumn, r.getColumn(), currentLine);
            }
            result.add(d);
        }
        //read the last char.
        r.read();

        if (!r.isAtEnd()) {
            //0 was returned, but the reader is not at end of file.
            throw new MultilineParseException("Document cannot contain the 0 char.",
                    r.getLine(), r.getColumn(), r.getLine(), r.getColumn());
        }

        return result;
    }

    /**
     * Reads a full document including the full line separator if available.
     */
    private Document parseDocument(
            EnhancedReader reader, DocumentFactory documentFactory,
            TagValueProvider tagValueProvider) throws IOException, MultilineParseException {

        String documentName = parseDocumentName(reader);
        expectComma(reader, reader.read());
        String documentType = parseDocumentType(reader);
        expectComma(reader, reader.read());
        int accessCount = parseAccessCount(reader);

        Document document;
        try {
            document = documentFactory.createDocument(
                    documentType, documentName, accessCount, tagValueProvider);
        } catch (TagException exception) {
            throw createMultilineParseException("document", exception.getMessage(), reader, 1);
        }

        if (document == null) {
            throw createMultilineParseException("document",
                    "Cannot create document of type '" + documentType + "'", reader, 1);
        }

        //read tag values. The comma is not read so far.
        while (true) {
            char c = reader.read();

            if (c == 0 || c == '\n' || c == '\r') {
                if (c == '\r' && reader.peek() == '\n') {
                    //read the full line separator.
                    reader.read();
                }
                break;
            }
            int startColumn = reader.getColumn();
            expectComma(reader, c);
            TagValue tagValue = parseTagValue(reader, tagValueProvider);

            try {
                document.addTagValue(tagValue);
            } catch (TagException exception) {
                throw createMultilineParseException("tag value for document",
                        exception.getMessage(), reader, startColumn);
            }
        }

        return document;
    }

    /**
     * Expects {@code c} to be a comma. Throws an exception if it is not a
     * comma.
     */
    private void expectComma(EnhancedReader reader, char c)
            throws MultilineParseException, IOException {
        if (c != ',') {
            String expected = Character.toString(c);
            //common special chars will be replaced, 
            //so they do not mess up the exception message.
            if (expected.equals("\n")) {
                expected = "\\n";
            }
            if (expected.equals("\r")) {
                expected = "\\r";
            }
            throw createMultilineParseException("document",
                    "Expected a comma, but got '" + expected + "'", reader, reader.getColumn() - 1);
        }
    }

    /**
     * Parses the document name. The document name will not be null or empty.
     * The document name can contain all characters except terminator characters
     * (see {@link #isTerminator}).
     */
    private String parseDocumentName(EnhancedReader reader)
            throws IOException, MultilineParseException {
        StringBuilder documentName = new StringBuilder();
        while (!isTerminator(reader.peek())) {
            documentName.append(reader.read());
        }

        String result = documentName.toString();
        if (result.isEmpty()) {
            throw createMultilineParseException("tag value", "The document name cannot be empty.",
                    reader, reader.getColumn());
        }
        return result;
    }

    /**
     * Parses the document type. The document type will not be null or empty.
     * The document type can contain only ascii letters.
     */
    private String parseDocumentType(EnhancedReader reader) throws MultilineParseException, IOException {
        StringBuilder documentName = new StringBuilder();
        while (!isTerminator(reader.peek())) {
            char c = reader.read();
            if (!isLetter(c)) {
                throw createMultilineParseException("document type",
                        "Expected a letter, but got '" + c + "'.",
                        reader, reader.getColumn() - 1);
            }
            documentName.append(c);
        }

        String result = documentName.toString();
        if (result.isEmpty()) {
            throw createMultilineParseException("document type", "The document type cannot be empty.",
                    reader, reader.getColumn());
        }
        return result;
    }

    /**
     * Parses the access count. The access count will be greater than or equal
     * to 0.
     */
    private int parseAccessCount(EnhancedReader reader)
            throws MultilineParseException, IOException {
        StringBuilder numberString = new StringBuilder();
        int startColumn = reader.getColumn();
        while (!isTerminator(reader.peek())) {
            char c = reader.read();
            if (!isDigit(c)) {
                throw createMultilineParseException("access count",
                        "Expected a digit, but got '" + c + "'.",
                        reader, reader.getColumn() - 1);
            }
            numberString.append(c);
        }

        try {
            int i = Integer.parseInt(numberString.toString());
            if (i >= 0) {
                return i;
            }
        } catch (NumberFormatException e) {
        }
        throw createMultilineParseException("access count",
                "Expected a number between 0 and " + Integer.MAX_VALUE + ", but got '" + numberString + "'.",
                reader, startColumn);
    }

    /**
     * Parses a tag value.
     */
    private TagValue parseTagValue(EnhancedReader reader,
            TagValueProvider tagValueProvider) throws MultilineParseException, IOException {

        String tagName = parseTagValueName(reader);

        int tagValueStartColumn = reader.getColumn();
        char c = reader.peek();

        String tagValue = ""; //The empty string will be parsed as "defined".

        if (c == '=') {
            reader.read(); //reads the '='
            tagValueStartColumn = reader.getColumn();
            StringBuilder tagValueStrBuilder = new StringBuilder();

            //the tag value can contain all characters except terminator characters.
            //tagValueProvider.getTagValue will add more constraints to the tag value.
            while (!isTerminator(reader.peek())) {
                c = reader.read();
                tagValueStrBuilder.append(c);
            }

            tagValue = tagValueStrBuilder.toString();
            if (tagValue.isEmpty()) {
                //the empty string is not allowed after '='
                throw createMultilineParseException("tag value", "The tag value cannot be empty.",
                        reader, tagValueStartColumn, tagValueStartColumn);
            }
        }

        try {
            return tagValueProvider.getTagValue(tagName, tagValue);
        } catch (ParseException e) {
            throw createMultilineParseException("tag value for tag '" + tagName + "'", e.getMessage(), reader,
                    tagValueStartColumn + e.getStartOffset(),
                    tagValueStartColumn + e.getEndOffset());
        }
    }

    /**
     * Reads a tag value name. The result will match
     * {@code [A-Za-z][A-Za-z0-9]*}. The next call to reader.read will return
     * the next character after the tag name.
     */
    private String parseTagValueName(EnhancedReader reader) throws MultilineParseException, IOException {
        StringBuilder tagName = new StringBuilder();

        char c = reader.peek();
        if (isTerminator(c)) {
            throw createMultilineParseException("tag name", "The tag name cannot be empty.",
                    reader, reader.getColumn(), reader.getColumn());
        }

        //the first character
        if (c != reader.read()) {
            throw new AssertionError();
        }
        if (!isLetter(c)) {
            throw createMultilineParseException("tag name",
                    "Expected a letter as first character, but got '" + c + "'",
                    reader, reader.getColumn() - 1);
        }
        tagName.append(c);

        //all other characters
        while (true) {
            c = reader.peek();
            if (isTerminator(c) || c == '=') {
                break;
            }
            if (c != reader.read()) {
                throw new AssertionError();
            }
            if (!isLetter(c) && !isDigit(c)) {
                throw createMultilineParseException("tag name",
                        "Expected a letter or digit, but got '" + c + "'",
                        reader, reader.getColumn() - 1);
            }

            tagName.append(c);
        }

        String result = tagName.toString();

        assert result.matches("[A-Za-z][A-Za-z0-9]*");
        return result;
    }

    private MultilineParseException createMultilineParseException(
            String category, String message, EnhancedReader reader, int startColumn) {
        return createMultilineParseException(
                category, message, reader, startColumn, reader.getColumn());
    }

    private MultilineParseException createMultilineParseException(
            String category, String message, EnhancedReader reader, int startColumn, int endColumn) {
        return createMultilineParseException(
                category, message, reader, startColumn, endColumn, reader.getLine());
    }

    private MultilineParseException createMultilineParseException(
            String category, String message, EnhancedReader reader, int startColumn, int endColumn, int line) {
        assert category != null;
        assert message != null;
        assert reader != null;
        assert startColumn > 0;
        assert endColumn >= startColumn;

        return new MultilineParseException(String.format("Error while parsing a %s: %s", category, message),
                line, startColumn, line, endColumn);
    }

    /**
     * Checks whether {@code c} is a terminator char. \r, \n, "," and 0 are
     * terminator chars.
     */
    private static boolean isTerminator(char c) {
        return c == 0 || c == '\r' || c == '\n' || c == ',';
    }

    /**
     * Checks whether c is an ascii digit.
     */
    private static boolean isDigit(char c) {
        return '0' <= c && c <= '9';
    }

    /**
     * Checks whether c is an ascii letter.
     */
    private static boolean isLetter(char c) {
        return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
    }
}
