package final2.de.hediet.documents;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.TagException;
import final2.de.hediet.tags.TagValueProvider;

/**
 * Represents an abstract chained document factory. Each concrete chained
 * document factory handles a specific document type and delegates the request
 * to the next document factory, if that factory is not null and the requested
 * document type cannot be handled by this instance. The comperation of document
 * types is case sensitive.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
abstract class ChainedDocumentFactory extends DocumentFactory {

    private final String documentTypeName;
    private final DocumentFactory nextDocumentFactory;

    /**
     * Creates a new chained document factory.
     *
     * @param documentTypeName the name of the document type to handle. The
     * comperation to this document type name will be case sensitive. Cannot be
     * null.
     * @param nextDocumentFactory the next document factory. Can be null.
     */
    public ChainedDocumentFactory(String documentTypeName, DocumentFactory nextDocumentFactory) {
        ArgumentExceptionHelper.ensureNotNull(documentTypeName, "documentTypeName");

        this.documentTypeName = documentTypeName;
        this.nextDocumentFactory = nextDocumentFactory;
    }

    /**
     * Creates a new document of the given document type.
     *
     * @param documentType the requested document type. Cannot be null.
     * @param documentName the name the returned document must have. Cannot be
     * null.
     * @param accessCount the number of accesses to the document. Has to be
     * greater than or equal to 0.
     * @param tagValueProvider the tag value provider the document can use to
     * create new tag values. Cannot be null.
     * @return the document. Will be null, if this factory and its chained
     * factory cannot handle the request.
     * @throws TagException will be thrown, if thrown by
     * {@link #createDocument(java.lang.String, int, de.hediet.final2.tags.TagValueProvider)}
     * or the chained factory.
     */
    @Override
    public final Document createDocument(String documentType, String documentName,
            int accessCount, TagValueProvider tagValueProvider) throws TagException {
        ArgumentExceptionHelper.ensureNotNull(documentType, "documentType");
        ArgumentExceptionHelper.ensureNotNull(documentName, "documentName");
        ArgumentExceptionHelper.ensureGreaterThanOrEqualTo(accessCount, 0, "accessCount");
        ArgumentExceptionHelper.ensureNotNull(tagValueProvider, "tagValueProvider");

        if (documentType.equals(documentTypeName)) {
            Document result = createDocument(documentName, accessCount, tagValueProvider);
            if (result == null) {
                throw new RuntimeException(
                        "The result of createDocument(String, int, TagValueProvider) cannot be null.");
            }
            return result;
        } else if (nextDocumentFactory != null) {
            return nextDocumentFactory.createDocument(
                    documentType, documentName, accessCount, tagValueProvider);
        } else {
            //This factory cannot handle documentTypeName and there is no next factory.
            return null;
        }
    }

    /**
     * Creates a new document. Will be called, if this factory can handle the
     * requested document type.
     *
     * @param documentName the name of the document. Cannot be null.
     * @param accessCount the number of access to the document. Have to be
     * greater than or equal to 0.
     * @param tagValueProvider the tag value provider. Cannot be null.
     * @return the document. Will not be null.
     * @throws TagException will be thrown, if a tag related exception occurs.
     */
    protected abstract Document createDocument(String documentName, int accessCount,
            TagValueProvider tagValueProvider) throws TagException;
}
