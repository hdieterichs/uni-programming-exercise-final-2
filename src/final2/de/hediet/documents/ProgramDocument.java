package final2.de.hediet.documents;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.ParseException;
import final2.de.hediet.TagException;
import final2.de.hediet.tags.Tag;
import final2.de.hediet.tags.TagValue;
import final2.de.hediet.tags.TagValueProvider;

/**
 * Represents an program document. This document uses the tag transformation
 * rules as described in the excercise sheet.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class ProgramDocument extends Document {

    private final TagValue executableDefined;

    /**
     * Creates a new text document. The executable defined tag value will be
     * obtained on construction.
     *
     * @param name the name of the document. Cannot be null.
     * @param accessCount the number of accesses to this document. Has to be
     * greater than or equal to 0.
     * @param tagValueProvider the provider which will be used to get the
     * executable defined tag value. Cannot be null.
     * @throws TagException will be thrown, if the executable defined tag value
     * cannot be created.
     */
    public ProgramDocument(String name, int accessCount, TagValueProvider tagValueProvider)
            throws TagException {
        super(name, accessCount);

        ArgumentExceptionHelper.ensureNotNull(tagValueProvider, "tagValueProvider");

        try {
            executableDefined = tagValueProvider.getTagValue("executable", "");
        } catch (ParseException p) {
            //only binary tags can parse the empty string.
            throw new TagException("Tag 'executable' is not a binary tag.");
        }
    }

    /**
     * Gets the tag value for the given tag. If the tag was not defined, the
     * undefined tag value will be returned. If the tag is equal to the
     * executable tag obtained on construction of this instance, the executable
     * defined tag value will be returned.
     *
     * @param tag the given tag. Cannot be null.
     * @return the tag value. Will not be null.
     */
    @Override
    public TagValue getTagValue(Tag tag) {
        ArgumentExceptionHelper.ensureNotNull(tag, "tag");

        if (tag.equals(executableDefined.getAssociatedTag())) {
            return executableDefined;
        }
        return super.getTagValue(tag);
    }

    /**
     * Creates a factory for this document. This factory will handle the
     * document type "program".
     *
     * @param nextDocumentFactory the next factory which will be used if this
     * factory cannot create the requested document. Can be null.
     * @return a factory for this document. Will not be null.
     */
    public static DocumentFactory createDocumentFactory(DocumentFactory nextDocumentFactory) {
        return new ChainedDocumentFactory("program", nextDocumentFactory) {
            @Override
            protected Document createDocument(String documentName,
                    int accessTimes, TagValueProvider tagValueProvider) throws TagException {
                return new ProgramDocument(documentName, accessTimes, tagValueProvider);
            }
        };
    }
}
