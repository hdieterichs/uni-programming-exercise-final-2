package final2.de.hediet;

/**
 * Represents a tag exception. This exceptoin will be thrown if a tag operation
 * fails.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class TagException extends Exception {

    /**
     * Creates a new tag exception.
     *
     * @param message the message used to describe the exception. Cannot be
     * null.
     */
    public TagException(String message) {
        super(message);

        ArgumentExceptionHelper.ensureNotNull(message, "message");
    }
}
