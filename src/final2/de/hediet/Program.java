package final2.de.hediet;

import final2.de.hediet.tags.NumericTag;
import final2.de.hediet.tags.BinaryTag;
import final2.de.hediet.tags.TagFactory;
import final2.de.hediet.tags.MultivalentTag;
import final2.de.hediet.documents.ImageDocument;
import final2.de.hediet.documents.AudioDocument;
import final2.de.hediet.documents.MultilineParseException;
import final2.de.hediet.documents.DocumentParser;
import final2.de.hediet.documents.ProgramDocument;
import final2.de.hediet.documents.DocumentFactory;
import final2.de.hediet.documents.TextDocument;
import final2.de.hediet.documents.VideoDocument;
import final2.de.hediet.documents.Document;
import final2.de.hediet.documents.DocumentContainer;
import final2.de.hediet.filesystem.Directory;
import final2.de.hediet.filesystem.DirectoryBuilder;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Collection;

/**
 * This class contains the entry point.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public final class Program {

    private Program() {
    }

    /**
     * The entry point.
     *
     * @param args the command line arguments. Should contain the filename of
     * the input file which will be processed. Cannot be null.
     */
    public static void main(String[] args) {
        ArgumentExceptionHelper.ensureNotNull(args, "args");

        try {
            if (args.length != 1) {
                throw new IllegalArgumentException(String.format(
                        "Command line arguments were expected to contain exactly 1 element, but got %d.", args.length));
            }

            FileReader fileReader = new FileReader(args[0]);
            try {
                DocumentContainer documentContainer = initializeDocumentContainer(fileReader);

                DirectoryBuilder dirBuilder = new DirectoryBuilder(System.out);
                Directory dir = dirBuilder.buildDirectory(
                        documentContainer.getDocumentCollection(), documentContainer.getTagCollection());
                System.out.println("---");
                System.out.println(dir.contentToString());
            } finally {
                fileReader.close();
            }
        } catch (IllegalArgumentException e) {
            if (!e.getMessage().contains("Command line")) {
                throw e;
            }
            System.err.println("Error, " + e.getMessage());
            System.exit(1);
        } catch (MultilineParseException | IOException e) {
            System.err.println("Error, " + e.getMessage());
            System.exit(1);
        }
    }

    /**
     * Creates an initialized document container. Supported tag types are
     * {@link BinaryTag}, {@link MultivalentTag} and {@link NumericTag}.
     * Supported document types are
     * {@link AudioDocument}, {@link ImageDocument}, {@link ProgramDocument}, {@link TextDocument}
     * and {@link VideoDocument}. The documents are loaded from {@code reader}.
     *
     * @param reader the reader used to parse the documents. Cannot be null.
     * @return the initialized document container. Will not be null.
     * @throws IOException will be thrown, if the reader throws an IOException.
     * @throws MultilineParseException will be thrown, if the stream which is
     * used by the reader is malformed.
     */
    public static DocumentContainer initializeDocumentContainer(Reader reader)
            throws IOException, MultilineParseException {
        ArgumentExceptionHelper.ensureNotNull(reader, "reader");

        DocumentContainer documentContainer = new DocumentContainer();

        Collection<TagFactory> c = documentContainer.getTagFactories();
        c.add(BinaryTag.getFactory());
        c.add(MultivalentTag.getFactory());
        c.add(NumericTag.getFactory());

        DocumentFactory documentFactory;
        documentFactory = AudioDocument.createDocumentFactory(null);
        documentFactory = ImageDocument.createDocumentFactory(documentFactory);
        documentFactory = ProgramDocument.createDocumentFactory(documentFactory);
        documentFactory = TextDocument.createDocumentFactory(documentFactory);
        documentFactory = VideoDocument.createDocumentFactory(documentFactory);

        DocumentParser parser = new DocumentParser();
        Collection<Document> documents = parser.parseDocuments(reader, documentFactory, documentContainer);
        documentContainer.getDocumentCollection().addAll(documents);

        return documentContainer;
    }
}
