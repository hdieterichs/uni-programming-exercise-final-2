package final2.de.hediet.tags;

import final2.de.hediet.ArgumentExceptionHelper;
import final2.de.hediet.ParseException;

/**
 * Represents an abstract tag.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public abstract class Tag {

    private final String name;

    /**
     * Creates a new tag.
     *
     * @param name the name of this tag. Cannot be null.
     */
    protected Tag(String name) {
        ArgumentExceptionHelper.ensureNotNull(name, "name");

        this.name = name;
    }

    /**
     * Parses {@code value}.
     *
     * @param value the value to parse. Can be null.
     * @return the parsed tag value. Will not be null.
     * @throws ParseException will be thrown, if {@code value} cannot be parsed.
     */
    public abstract TagValue parse(String value) throws ParseException;

    /**
     * Gets the name.
     *
     * @return the name. Will not be null.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets a undefined tag value of this tag.
     *
     * @return the undefined tag value.
     */
    public UndefinedTagValue getUndefinedTagValue() {
        return new UndefinedTagValue();
    }

    /**
     * Gets a string representation of this object.
     *
     * @return the string representation of this object. Will not be null.
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + " " + getName();
    }

    /**
     * Represents an undefined tag value.
     */
    public final class UndefinedTagValue extends TagValue {

        private UndefinedTagValue() {
        }

        @Override
        public Tag getAssociatedTag() {
            return Tag.this;
        }

        @Override
        public String getValueAsString() {
            return "undefined";
        }
    }
}
