package final2.de.hediet.tags;

/**
 * Represents an abstract tag factory.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public abstract class TagFactory {

    /**
     * Checks whether the tag can parse {@code value}. If {@code value} can be
     * parsed, the tag created by this factory will be able to parse
     * {@code value} also.
     *
     * @param value the value to parse. Can be null.
     * @return true, if {@code value} can be parsed, otherwise false.
     */
    public abstract boolean canParseValue(String value);

    /**
     * Creates the tag.
     *
     * @param name the name of the tag. Cannot be null.
     * @return the created tag. Will not be null.
     */
    public abstract Tag createTag(String name);
}
