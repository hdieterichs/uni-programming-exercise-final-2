package final2.de.hediet.tags;

import final2.de.hediet.ParseException;

/**
 * Represents a tag value provider.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public interface TagValueProvider {

    /**
     * Gets a tag value.
     *
     * @param tagName the name of the tag owning the requested tag value. Cannot
     * be null.
     * @param value the value of the tag value. Can be null.
     * @return the tag value. Will not be null.
     * @throws ParseException will be thrown, if {@code value} cannot be parsed.
     */
    public TagValue getTagValue(String tagName, String value) throws ParseException;
}
