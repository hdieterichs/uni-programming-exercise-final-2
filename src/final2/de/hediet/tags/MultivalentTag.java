package final2.de.hediet.tags;

import final2.de.hediet.ParseException;

/**
 * Represents a multivalent tag.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public class MultivalentTag extends Tag {

    private static final String VALID_VALUE_REGEX = "[A-Za-z][A-Za-z0-9 ]*";

    /**
     * Creates a new multivalent tag.
     *
     * @param name the name of this tag. Cannot be null.
     */
    public MultivalentTag(String name) {
        super(name);
    }

    /**
     * Parses {@code value}.
     *
     * @param value the value to parse. Can be null.
     * @return the parsed tag value. Will not be null.
     * @throws ParseException will be thrown, if {@code value} cannot be parsed.
     */
    @Override
    public TagValue parse(String value) throws ParseException {
        if (value == null) {
            throw new ParseException("Cannot parse null-string", 0, 0);
        }

        if (!value.matches(VALID_VALUE_REGEX)) {
            throw new ParseException("'" + value + "' is not a valid multivalent tag.", 0, value.length());
        }

        return new MultivalentTagValue(value);
    }

    /**
     * Gets a tag factory of this tag.
     *
     * @return the tag factory. Will not be null.
     */
    public static TagFactory getFactory() {
        return new MultivalentTagFactory();
    }

    /**
     * Represents a multivalent tag value.
     */
    public final class MultivalentTagValue extends TagValue {

        private final String value;

        private MultivalentTagValue(String value) {
            this.value = value;
        }

        /**
         * Gets the tag associated to this tag value.
         *
         * @return the tag associated to this tag value. Will not be null.
         */
        @Override
        public Tag getAssociatedTag() {
            return MultivalentTag.this;
        }

        /**
         * Gets the value as string.
         *
         * @return the value as string. Can be null.
         */
        @Override
        public String getValueAsString() {
            return value;
        }
    }

    private static final class MultivalentTagFactory extends TagFactory {

        private MultivalentTagFactory() {
        }

        @Override
        public boolean canParseValue(String value) {
            return value != null && value.matches(VALID_VALUE_REGEX);
        }

        @Override
        public Tag createTag(String name) {
            return new MultivalentTag(name);
        }

    }
}
