package final2.de.hediet.tags;

import java.util.Objects;

/**
 * Represents an abstract tag value. A tag value maps a value to a tag. Tag
 * values can be created only by the owning tag.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public abstract class TagValue {

    /**
     * Creates a new tag value.
     */
    protected TagValue() {
    }

    /**
     * Gets a hashcode of this object.
     *
     * @return the hashcode.
     */
    @Override
    public int hashCode() {
        int hash = 19;
        hash = hash * 31 + getClass().toString().hashCode();
        hash = hash * 31 + getAssociatedTag().hashCode();
        hash = hash * 31 + getValueAsString().hashCode();

        return hash;
    }

    /**
     * Checks whether this instance is equal to {@code obj}.
     *
     * @param obj the object to compare.
     * @return true, if they are equal, otherwise false.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TagValue other = (TagValue) obj;

        return this.getAssociatedTag() == other.getAssociatedTag()
                && Objects.equals(this.getValueAsString(), other.getValueAsString());
    }

    /**
     * Gets the associated tag.
     *
     * @return the associated tag. Will not be null.
     */
    public abstract Tag getAssociatedTag();

    /**
     * Gets the value as string.
     *
     * @return the value as string. Will not be null.
     */
    public abstract String getValueAsString();

    /**
     * Gets a string representation of this object.
     *
     * @return a string representation of this object. Will not be null.
     */
    @Override
    public String toString() {
        return getAssociatedTag() + " = " + getValueAsString();
    }
}
