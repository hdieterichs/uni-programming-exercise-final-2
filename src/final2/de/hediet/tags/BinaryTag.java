package final2.de.hediet.tags;

import final2.de.hediet.ParseException;

/**
 * Represents a binary tag.
 * 
 * @author Henning Dieterichs
 * @version 1.0
 */
public class BinaryTag extends Tag {

    /**
     * Creates a new binary tag.
     * 
     * @param name the name of the tag. Cannot be null.
     */
    public BinaryTag(String name) {
        super(name);
    }

    /**
     * Gets a defined tag value owned by this tag.
     * 
     * @return the defined tag value.
     */
    public TagValue createDefinedTagValue() {
        return new DefinedTagValue();
    }

    /**
     * Parses {@code value}.
     * 
     * @param value the value to parse. Can be null.
     * @return the parsed tag value. Will not be null.
     * @throws ParseException will be thrown, if {@code value} cannot be parsed.
     */
    @Override
    public TagValue parse(String value) throws ParseException {
        if (value == null) {
            return getUndefinedTagValue();
        } else if (value.equals("")) {
            return new DefinedTagValue();
        } else {
            throw new ParseException("'" + value + "' is not a valid binary tag value.", 0, value.length());
        }
    }

    /**
     * Gets a tag factory of this tag.
     * 
     * @return the tag factory. Will not be null.
     */
    public static TagFactory getFactory() {
        return new BinaryTagFactory();
    }

    /**
     * Represents a defined tag value.
     */
    public final class DefinedTagValue extends TagValue {

        private DefinedTagValue() {
        }

        /**
         * Gets the tag associated to this tag value.
         * 
         * @return the tag associated to this tag value. Will not be null.
         */
        @Override
        public Tag getAssociatedTag() {
            return BinaryTag.this;
        }

        /**
         * Gets the value as string.
         * 
         * @return the value as string. Can be null.
         */
        @Override
        public String getValueAsString() {
            return "defined";
        }
    }

    private static final class BinaryTagFactory extends TagFactory {

        private BinaryTagFactory() {
        }
        
        @Override
        public boolean canParseValue(String value) {
            return value == null || value.equals("");
        }

        @Override
        public Tag createTag(String name) {
            return new BinaryTag(name);
        }
    }
}
