package final2.de.hediet.tags;

import final2.de.hediet.ParseException;

/**
 * Represents a binary tag.
 * 
 * @author Henning Dieterichs
 * @version 1.0
 */
public class NumericTag extends Tag {

    /**
     * Creates a new numeric tag.
     * 
     * @param name the name of this tag. Cannot be null.
     */
    public NumericTag(String name) {
        super(name);
    }

    /**
     * Gets a numeric tag value owned by this tag.
     * 
     * @param value the value of the tag value.
     * @return the tag value.
     */
    public TagValue createTagValue(int value) {
        return new NumericTagValue(value);
    }

    /**
     * Parses {@code value}.
     * 
     * @param value the value to parse. Can be null.
     * @return the parsed tag value. Will not be null.
     * @throws ParseException will be thrown, if {@code value} cannot be parsed.
     */
    @Override
    public TagValue parse(String value) throws ParseException {
        if (value == null || value.isEmpty()) {
            throw new ParseException("The empty string is not a valid numeric tag value.", 0, 0);
        }
        try {
            int i = Integer.parseInt(value);
            return createTagValue(i);
        } catch (NumberFormatException e) {
            throw new ParseException("'" + value + "' is not a valid numeric tag value.", 0, value.length());
        }
    }

    /**
     * Gets a tag factory of this tag.
     * 
     * @return the tag factory. Will not be null.
     */
    public static TagFactory getFactory() {
        return new NumericTagFactory();
    }

    /**
     * Represents a numeric tag value.
     */
    public final class NumericTagValue extends TagValue {

        private final int value;

        private NumericTagValue(int value) {
            this.value = value;
        }

        /**
         * Gets the tag associated to this tag value.
         * 
         * @return the tag associated to this tag value. Will not be null.
         */
        @Override
        public Tag getAssociatedTag() {
            return NumericTag.this;
        }

        /**
         * Gets the value as string.
         * 
         * @return the value as string. Can be null.
         */
        @Override
        public String getValueAsString() {
            return Integer.toString(value);
        }

        /**
         * Gets the value.
         * 
         * @return the value.
         */
        public int getValue() {
            return value;
        }
    }

    private static final class NumericTagFactory extends TagFactory {

        private NumericTagFactory() {
        }

        @Override
        public boolean canParseValue(String value) {
            if (value == null) {
                return false;
            }
            try {
                Integer.parseInt(value);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }

        @Override
        public Tag createTag(String name) {
            return new NumericTag(name);
        }
    }
}
