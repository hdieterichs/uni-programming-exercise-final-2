package final2.de.hediet;

/**
 * An helper class for argument exceptions.
 *
 * @author Henning Dieterichs
 * @version 1.0
 */
public final class ArgumentExceptionHelper {

    private ArgumentExceptionHelper() {
        throw new AssertionError("Utility Classes must not be instantiated!");
    }

    /**
     * Ensures that <code>argument</code> is not null and throws an
     * {@link IllegalArgumentException} if it is null.
     *
     * @param argument the argument to check.
     * @param argumentName the argument name which will be used within the
     * exception.
     */
    public static void ensureNotNull(Object argument, String argumentName) {
        if (argument == null) {
            throw new IllegalArgumentException(
                    String.format("Argument %s cannot be null.", argumentName));
        }
    }

    /**
     * Ensures that <code>argument</code> is greater than or equal to
     * <code>greaterThanOrEqualTo</code> and throws an
     * {@link IllegalArgumentException} if it is not.
     *
     * @param argument the argument to check.
     * @param greaterThanOrEqualTo the value to compare against.
     * @param argumentName the argument name which will be used within the
     * exception.
     */
    public static void ensureGreaterThanOrEqualTo(int argument,
            int greaterThanOrEqualTo, String argumentName) {
        if (argument < greaterThanOrEqualTo) {
            throw new IllegalArgumentException(
                    String.format("Argument %s has to be greater than or equal to %d, but was %d.",
                            argumentName, greaterThanOrEqualTo, argument));
        }
    }
}
